"""Load .dat files into memory
"""
#-------------------------------------------------------------------------
# Name:        photomodeler
# Purpose:      Functions for working with Photomodeler software and data
# Author:      Alan Fleming
# Version:      0.1
# Created:     05/03/2015
# Copyright:   (c) Alan Fleming 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------
#!/usr/bin/env python
__metaclass__ = type

import numpy as np

__all__ = ["load3Ddata"]


def load3Ddata(loadfile, time, delay, framerate, latency,
               firstEpoch=0, tightnessLimit=0.5, minPoints=50):
    """ load photomodeler 3D point data into a dictonary by 'Point ID' with data
    converted to timeseries data.
    loadfile    : photomodeler .txt file
    time        : time array values to map against
    delay       : delay in time units to the "firstEpoch"
    framerate   : number of Epochs per second
    latency     : to account for exposure and delay in image acquisition
    tightnessLimit: any 'Point ID' with tightness > tightnessLimit will be dropped
    minPoints   : any 'Point ID' with less than minPoints will be dropped

    returns data, shortest_epoch

    data: dictionary of timeseries points
    shortest_epoch: the minimum number of frames

    """

    samplerate = 1 / (time[1] - time[0])

    # Determines the index of the image relative to the dat time series data
    def getindex(frame): return np.array((frame - firstEpoch) * samplerate / framerate
                                         + samplerate * delay + latency, dtype=int)

    # points from photogrammetry file
    #-------------------------------------------------------------------------

    with open(loadfile) as f:

        # header info
        line = f.readline()
        assert line.find('Project') == 0, "Incorrect file format!"
        units = f.readline().split(':')[1].strip()
        names = "Point ID", "Epoch Number", "X", "Y", "Z", "Tightness", "Delta"
        f.readline()

    # data
        data = np.fromfile(f, sep=' ',)
        data = data.reshape(data.size / len(names), len(names))

    # make a record array
        rec = np.rec.array([0, 0, 0.0, 0.0, 0.0, 0.0, 0.0], names=names)
        rec.resize(len(data))

        for i, n in enumerate(names):
            rec[n] = data[:, i]

    # datapoints
        points = np.unique(rec['Point ID'])

    # shortlist quality point data
    #-------------------------------------------------------------------------
    shortest_epoch = max(rec['Epoch Number'])
    point_shortlist = []

    for p in points:

        r = rec['Point ID'] == p

        print(r.sum())
        if r.sum() < minPoints:
            continue

        if any(rec['Tightness'][r] > tightnessLimit):
            continue

        point_shortlist.append(p)
        shortest_epoch = min(rec['Epoch Number'][r][-1], shortest_epoch)

    names = 'Time', 'X', 'Y', 'Z', 'Tightness', 'Delta'

    # build a timeseries array for each point
    data = {}

    # convert point epoch data to timeseries data and store in a dictionary
    #-------------------------------------------------------------------------

    for p in point_shortlist:

        r = rec['Point ID'] == p
        d = rec[r]

        # time values
        i = getindex(d['Epoch Number'])
        i = i[i < time.size]
        t = time[i]

        d = d[:i.size]

        data_out = np.rec.array([0.0] * len(names), names=names)
        data_out.resize(i.size)

        for k in names:
            if k == 'Time':
                data_out[k] = t
            else:
                data_out[k] = d[k]

        # store by 'Point ID'
        data[p] = np.ma.array(data_out)

    return data, shortest_epoch
